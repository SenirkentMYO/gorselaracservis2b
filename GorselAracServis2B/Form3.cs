﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselAracServis2B
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AracServisEntities database = new AracServisEntities();
            List<Randevu_Bil> RandevuListesi = database.Randevu_Bil.ToList();
            dataGridView2.DataSource = RandevuListesi;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form1 frm1 = new Form1();
            this.Close();
            frm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form5 frm5 = new Form5();
            this.Hide();
            frm5.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AracServisEntities database = new AracServisEntities();
            List<Randevu_Bil> MusteriListesi = database.Randevu_Bil.Where(b => b.Plaka.Contains(textBox1.Text)).ToList();
            dataGridView2.DataSource = MusteriListesi;
        }
    }
}
