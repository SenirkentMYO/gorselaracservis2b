﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GorselAracServis2B
{
    public partial class Form2 : Form
    {
        
        
        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form4 frm4 = new Form4();
            this.Hide();
            frm4.Show();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            AracServisEntities database = new AracServisEntities();
            List<mus_bil> MusteriListesi = database.mus_bil.ToList();
            dataGridView1.DataSource = MusteriListesi;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form1 frm2 = new Form1();
            this.Close();
            frm2.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AracServisEntities database = new AracServisEntities();
            List<mus_bil> MusteriListesi = database.mus_bil.Where(b => b.Ad.Contains(textBox1.Text)).ToList();
            dataGridView1.DataSource = MusteriListesi;
          
        }
    }
}
